FROM ruby:2.5-alpine

RUN apk add --no-cache --update \
  tzdata \
  build-base \
  busybox \
  ca-certificates \
  curl \
  curl-dev \
  git \
  graphicsmagick \
  libffi-dev \
  libsodium-dev \
  openssh-client \
  postgresql-dev \
  postgresql-client \
  rsync \
  yarn
